package YBClient

import (
	"errors"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"

	"youbei/dao"
)

// ResAPI ...
type ResAPI struct {
	Success int         `json:"success"`
	Msg     string      `json:"msg"`
	Result  interface{} `json:"result"`
}

func APIReturn(c *gin.Context, Success int, Msg string, data interface{}) {
	res := gin.H{
		"success": Success,
		"msg":     Msg,
		"result":  data,
	}
	c.AbortWithStatusJSON(Success, res)
}

func Cors() gin.HandlerFunc {
	return func(c *gin.Context) {
		method := c.Request.Method
		c.Header("Access-Control-Allow-Origin", "*")
		c.Header("Access-Control-Allow-Headers", "Content-Type,AccessToken,X-CSRF-Token, Authorization, Token")
		c.Header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE")
		c.Header("Access-Control-Expose-Headers", "Content-Length, Access-Control-Allow-Origin, Access-Control-Allow-Headers, Content-Type")
		c.Header("Access-Control-Allow-Credentials", "true")
		if method == "OPTIONS" {
			c.AbortWithStatus(http.StatusNoContent)
		}
		c.Next()
	}
}

func GetQueryParams(c *gin.Context) map[string]any {
	query := c.Request.URL.Query()
	var queryMap = make(map[string]any, len(query))
	for k := range query {
		queryMap[k] = c.Query(k)
	}
	return queryMap
}

func GetRestul(c *gin.Context, sqlresult string, v interface{}) (int64, error) {
	if err := GetRestulList(c, sqlresult, v); err != nil {
		return 0, err
	}

	total, err := GetRestulTotal(c, sqlresult)
	if err != nil {
		return 0, err
	}

	return total, nil
}

func GetRestulTotal(c *gin.Context, sqlresult string) (int64, error) {
	maps := GetQueryParams(c)
	delete(maps, "pageSize")
	count, err := dao.Localdb().SqlTemplateClient(sqlresult, &maps).Query().Count()
	if err != nil {
		return int64(0), err
	}
	return int64(count), nil
}

func GetRestulList(c *gin.Context, sqlresult string, v interface{}) error {
	maps := GetQueryParams(c)
	if err := dao.Localdb().SqlTemplateClient(sqlresult, &maps).Find(v); err != nil {
		return err
	}
	return nil
}

func getwl() (dao.Whitelist, error) {
	wl := dao.Whitelist{}
	if bol, err := dao.Localdb().Get(&wl); err != nil {
		return wl, err
	} else {
		if !bol {
			return wl, errors.New("为空")
		}
	}
	return wl, nil
}

func Prepare() gin.HandlerFunc {
	return func(c *gin.Context) {
		tokenHeader := c.GetHeader("token")
		if tokenHeader == "" {
			APIReturn(c, 500, "token不能为空", errors.New("token不能为空"))
			return
		}
		wl, err := getwl()
		if err != nil {
			APIReturn(c, 500, "token获取失败", errors.New("token获取失败"))
			return
		}
		fmt.Println(tokenHeader, wl.Token)
		if wl.Token != tokenHeader {
			APIReturn(c, 500, "token不匹配", errors.New("token不匹配"))
			return
		}
	}
}
