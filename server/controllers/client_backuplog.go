package controllers

import (
	"fmt"
	"youbei/dao"

	"github.com/gin-gonic/gin"
)

type respLog struct {
	Cols []string `json:"cols"`
	Log  dao.Log  `json:"log"`
}

func UpdateLog(c *gin.Context) {
	resplog := respLog{}
	if err := c.Bind(&resplog); err != nil {
		APIReturn(c, 500, "失败", err)
	}
	log := resplog.Log
	if _, err := dao.Localdb().ID(log.ID).Cols(resplog.Cols...).Update(&log); err != nil {
		APIReturn(c, 500, "失败", err)
	}
	APIReturn(c, 200, "成功", nil)
}

type respRLog struct {
	Cols []string `json:"cols"`
	RLog dao.Rlog `json:"rlog"`
}

func UpdateRLog(c *gin.Context) {
	resprlog := respRLog{}
	if err := c.Bind(&resprlog); err != nil {
		APIReturn(c, 500, "失败", err)
	}
	log := resprlog.RLog
	if _, err := dao.Localdb().ID(log.ID).Cols(resprlog.Cols...).Update(&log); err != nil {
		APIReturn(c, 500, "失败", err)
	}
	APIReturn(c, 200, "成功", nil)
}

func AddYSLog(c *gin.Context) {
	yslogs := dao.YsUploadFile{}
	if err := c.Bind(&yslogs); err != nil {
		fmt.Println(err.Error())
		APIReturn(c, 500, "失败1", err)
		return
	}
	if _, err := dao.Localdb().Insert(&yslogs); err != nil {
		fmt.Println(err.Error())
		APIReturn(c, 500, "失败2", err)
		return
	}
	APIReturn(c, 200, "成功", nil)
}

func AddYSpacketLog(c *gin.Context) {
	yspackets := []dao.YsPacket{}
	if err := c.Bind(&yspackets); err != nil {
		APIReturn(c, 500, "失败", err)
	}
	if len(yspackets) > 0 {
		for _, v := range yspackets {
			dao.Localdb().Insert(&v)
		}
	}
	APIReturn(c, 200, "成功", nil)
}

type respPacketLog struct {
	Cols      []string     `json:"cols"`
	PacketLog dao.YsPacket `json:"packetlog"`
}

func UpdateYSpacketLog(c *gin.Context) {
	resplog := respPacketLog{}
	if err := c.Bind(&resplog); err != nil {
		APIReturn(c, 500, "失败", err)
	}
	log := resplog.PacketLog
	if _, err := dao.Localdb().ID(log.ID).Cols(resplog.Cols...).Update(&log); err != nil {
		APIReturn(c, 500, "失败", err)
	}
	APIReturn(c, 200, "成功", nil)
}
