package YouBeiStorage

import (
	"errors"

	"youbei/dao"

	"github.com/gin-gonic/gin"
)

// GetYserverLog ...
func GetYserverLog(c *gin.Context) {
	id := c.Param("id")
	yfinfo := new(dao.YserverFile)
	if bol, err := dao.Localdb().ID(id).Get(yfinfo); err != nil {
		APIReturn(c, 500, "获取日志失败", err.Error())
		return
	} else {
		if !bol {
			APIReturn(c, 500, "日志不存在22222", errors.New("日志不存在xxxxx"))
			return
		}
	}

	ypsinfo := []dao.YserverPacket{}
	if err := dao.Localdb().Where("fid=?", yfinfo.ID).Asc("sort").Find(&ypsinfo); err != nil {
		APIReturn(c, 500, "获取备份信息失败", err.Error())
		return
	}

	yfinfo.YserverPackets = ypsinfo
	APIReturn(c, 200, "成功", yfinfo)
}
