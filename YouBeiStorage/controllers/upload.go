package YouBeiStorage

import (
	"errors"
	"os"
	"strconv"
	"youbei/utils/config"

	"youbei/dao"

	"github.com/gin-gonic/gin"
)

// uploadfile ...
type uploadfile struct {
	ID        string `json:"id"`
	FileName  string `json:"filename"`
	SaveDir   string `json:"savedir"`
	Size      int64  `json:"size"`
	PacketNum int64  `json:"packetnum"`
}

//EnableServer 启动本地存储
func EnableServer(c *gin.Context) {
	ob := new(dao.Yserver)
	if err := c.Bind(ob); err != nil {
		APIReturn(c, 500, "解析失败", err.Error())
		return
	}

	if err := ob.EnableYserver(); err != nil {
		APIReturn(c, 500, "服务启动失败", err.Error())
		return
	}

	APIReturn(c, 200, "服务启动成功", nil)
}

//Yserverlist 本地存储查询
func Yserverlist(c *gin.Context) {
	ys := dao.Yserver{}
	if bol, err := dao.Localdb().Get(&ys); err != nil {

		APIReturn(c, 500, "查询Yserver失败", err.Error())
		return
	} else {
		if !bol {
			APIReturn(c, 500, "Yserver不存在", errors.New("Yserver不存在"))
			return
		}
	}

	ys.Port = 8080
	APIReturn(c, 200, "获取成功", ys)
}

//UploadFile 新增上传文件任务
func UploadFile(c *gin.Context) {
	// 加载配置文件
	conf, err := config.LoadConfigFromFile()
	if err != nil {
		APIReturn(c, 500, "加载配置文件失败", err)
		return
	}

	if c.Request.PostFormValue("username") != conf.ConfYserver.User || c.Request.PostFormValue("password") != conf.ConfYserver.Password {
		APIReturn(c, 500, "账号密码错误", errors.New("账号密码错误"))
		return
	}
	var ob uploadfile

	ob.ID = c.Param("id")
	ob.FileName = c.Request.PostFormValue("filename")
	if size, err := strconv.ParseInt(c.Request.PostFormValue("size"), 10, 64); err != nil {
		APIReturn(c, 500, "获取文件大小失败", err.Error())
		return
	} else {
		ob.Size = size
	}
	ob.SaveDir = c.Request.PostFormValue("savedir")

	if size, err := strconv.ParseInt(c.Request.PostFormValue("packetnum"), 10, 64); err != nil {
		APIReturn(c, 500, "获取文件切片数失败", err.Error())
		return
	} else {
		ob.PacketNum = size
	}
	dirpath := "."

	if conf.ConfYserver.Savepath != "" {
		dirpath = conf.ConfYserver.Savepath
	}

	// 创建文件保存目录

	os.MkdirAll(dirpath+"/"+ob.SaveDir, os.ModeDir)
	filedist := dirpath + "/" + ob.SaveDir + "/" + ob.FileName
	f, err := os.Create(filedist)
	if err != nil {
		APIReturn(c, 500, "文件创建失败", err.Error())
		return
	}

	// 关闭文件句柄
	defer f.Close()

	// 分配文件大小
	if err := f.Truncate(ob.Size); err != nil {
		APIReturn(c, 500, "文件填充失败", err.Error())
		return
	}

	if err := dao.AddFile(ob.ID, ob.FileName, ob.SaveDir, ob.Size, ob.PacketNum); err != nil {
		APIReturn(c, 500, "录入失败", err.Error())
		return
	}

	APIReturn(c, 200, "录入成功", nil)
	return
}

//Uploadpacket 接收上传分片包
func Uploadpacket(c *gin.Context) {
	conf, err := config.LoadConfigFromFile()
	if err != nil {
		APIReturn(c, 500, "加载配置文件失败", err)
		return
	}

	id := c.Param("id")
	// 获取文件保存路径
	savepath := "."
	if conf.ConfYserver.Savepath != "" {
		savepath = conf.ConfYserver.Savepath
	}

	// 获取偏移量
	offset, err := strconv.ParseInt(c.Param("offset"), 10, 64)
	if err != nil {
		APIReturn(c, 500, "获取offset失败", err.Error())
		return
	}

	// 打开文件进行读写
	f, err := os.OpenFile(savepath, os.O_RDWR, os.ModePerm)
	if err != nil {
		APIReturn(c, 500, "打开文件失败", err.Error())
		return
	}

	defer f.Close()
	// 写入分片数据
	if body, err := c.GetRawData(); err != nil {
		APIReturn(c, 500, "接收数据失败", err.Error())
		return
	} else {
		if _, err := f.WriteAt(body, offset); err != nil {
			APIReturn(c, 500, "打开写入失败", err.Error())
			return
		}
	}

	APIReturn(c, 200, id, nil)

}

//UploadpacketDone 接收上传分片包
func UploadpacketDone(c *gin.Context) {
	id := c.Param("id")
	status := c.GetInt("status")
	if err := dao.FinshFile(id, status); err != nil {
		APIReturn(c, 500, "完成失败", err.Error())
		return
	}

	APIReturn(c, 200, "执行成功", nil)
}

//Uploadlogs 存储日志查询
func Uploadlogs(c *gin.Context) {
	type Query struct {
		Page  int `form:"page"`
		Count int `form:"count"`
	}

	query := Query{}
	if err := c.Bind(&query); err != nil {
		APIReturn(c, 500, "解析失败", err.Error())
		return
	}

	yps, err := dao.AllFile(query.Page, query.Count)
	if err != nil {
		APIReturn(c, 500, "查询失败1", err.Error())
		return
	}

	total, err := dao.Filecount()
	if err != nil {
		APIReturn(c, 500, "查询总数失败", err.Error())
		return
	}

	rep := map[string]interface{}{"count": total, "data": yps}
	APIReturn(c, 200, "查询成功", &rep)
}
